package com.tubemate.tubebox.launch.splash;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.launch.home.HomeActivity;
import com.tubemate.tubebox.launch.slider.SliderActivity;
import com.tubemate.tubebox.util.BaseActivity;
import com.tubemate.tubebox.util.PrefManager;

public class SplashActivity extends BaseActivity {
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        prefManager = new PrefManager(this);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, (!prefManager.getBoolean(PrefManager.FIRST_TIME_LAUNCH, true))?HomeActivity.class:SliderActivity.class));
                finish();
            }
        }, 3000);
    }
}
