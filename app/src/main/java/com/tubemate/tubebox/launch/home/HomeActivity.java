package com.tubemate.tubebox.launch.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.launch.movie.MovieActivity;
import com.tubemate.tubebox.launch.player.PlayerActivity;
import com.tubemate.tubebox.util.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HomeActivity extends BaseActivity{
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private List<Integer> sliders;
    private int currentPage = 0;
    private LinearLayout movies;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        viewPager = findViewById(R.id.viewPager);
        indicator = findViewById(R.id.indicator);

        sliders = new ArrayList<>();
        sliders.add(R.drawable.banner1);
        sliders.add(R.drawable.banner2);
        sliders.add(R.drawable.banner3);
        sliders.add(R.drawable.banner4);
        sliders.add(R.drawable.banner5);

        viewPager.setAdapter(new Adapter(this,sliders));
        indicator.setViewPager(viewPager);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == sliders.size()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);

        movies=findViewById(R.id.movies);

        movies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,MovieActivity.class));
            }
        });
    }

    public void movie(View view) {
        startActivity(new Intent(HomeActivity.this, PlayerActivity.class));
    }

    private class Adapter extends PagerAdapter {
        private final LayoutInflater inflater;
        private Context context;
        private List<Integer> sliders;

        Adapter(Context context, List<Integer> sliders) {
            this.context = context;
            this.sliders = sliders;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return sliders.size();
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View myImageLayout = inflater.inflate(R.layout.item_poster, container, false);
            ImageView myImage = myImageLayout.findViewById(R.id.image);
            myImage.setImageDrawable(getResources().getDrawable(sliders.get(position)));
            container.addView(myImageLayout, 0);
            return myImageLayout;
        }

    }
}
