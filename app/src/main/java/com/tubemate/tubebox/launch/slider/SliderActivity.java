package com.tubemate.tubebox.launch.slider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.launch.auth.SigninActivity;
import com.tubemate.tubebox.launch.auth.SignupActivity;
import com.tubemate.tubebox.launch.home.HomeActivity;
import com.tubemate.tubebox.util.BaseActivity;
import com.tubemate.tubebox.util.PrefManager;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class SliderActivity extends BaseActivity {
    private PrefManager prefManager;

    private ViewPager viewPager;
    private CircleIndicator indicator;

    private List<Slider> sliders;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        if(!prefManager.getBoolean(PrefManager.FIRST_TIME_LAUNCH, true)){
            startActivity(new Intent(SliderActivity.this, HomeActivity.class));
            finish();
        }

        setContentView(R.layout.activity_slider);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        viewPager = findViewById(R.id.viewPager);
        indicator = findViewById(R.id.indicator);

        sliders = new ArrayList<>();
        sliders.add(new Slider(R.drawable.slider11,"Enjoy Blockbuster Movies from Bollywood, Tollywood and more...",R.color.slideText1));
        sliders.add(new Slider(R.drawable.slider12,"Listen to Hindi, Tamil, Telugu and Malayalam Music for Free",R.color.slideText2));
        sliders.add(new Slider(R.drawable.slider13,"Popular Indian & International TV Shows",R.color.slideText3));

        viewPager.setAdapter(new Adapter(this,sliders));
        indicator.setViewPager(viewPager);
    }

    private class Adapter extends PagerAdapter {
        private final LayoutInflater inflater;
        private Context context;
        private List<Slider> sliders;

        Adapter(Context context, List<Slider> sliders) {
            this.context = context;
            this.sliders = sliders;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return sliders.size();
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View myImageLayout = inflater.inflate(R.layout.item_slider, container, false);
            LinearLayout myImage = myImageLayout.findViewById(R.id.root);
            TextView textView = myImageLayout.findViewById(R.id.textView);
            Button signin = myImageLayout.findViewById(R.id.buttonSignin);
            Button signup = myImageLayout.findViewById(R.id.buttonSignup);
            TextView skip = myImageLayout.findViewById(R.id.textSkip);
            myImage.setBackground(getResources().getDrawable(sliders.get(position).getImage()));
            textView.setText(sliders.get(position).getText());
//            textView.setTextColor(ContextCompat.getColor(context,sliders.get(position).getTextColor()));
            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    prefManager.putBoolean(PrefManager.FIRST_TIME_LAUNCH,false);
                    startActivity(new Intent(context,SigninActivity.class));
                    finish();
                }
            });
            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    prefManager.putBoolean(PrefManager.FIRST_TIME_LAUNCH,false);
                    startActivity(new Intent(context,SignupActivity.class));
                    finish();
                }
            });
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    prefManager.putBoolean(PrefManager.FIRST_TIME_LAUNCH,false);
                    startActivity(new Intent(context,HomeActivity.class));
                    finish();
                }
            });
            container.addView(myImageLayout, 0);
            return myImageLayout;
        }
    }
}
