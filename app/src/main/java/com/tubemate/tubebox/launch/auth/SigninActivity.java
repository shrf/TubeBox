package com.tubemate.tubebox.launch.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.launch.home.HomeActivity;
import com.tubemate.tubebox.util.BaseActivity;

public class SigninActivity extends BaseActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        Button signin = findViewById(R.id.buttonSignin);
        Button facebook = findViewById(R.id.facebook);
        Button google = findViewById(R.id.google);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SigninActivity.this, HomeActivity.class));
                finish();
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SigninActivity.this, HomeActivity.class));
                finish();
            }
        });

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SigninActivity.this, HomeActivity.class));
                finish();
            }
        });
    }
}
