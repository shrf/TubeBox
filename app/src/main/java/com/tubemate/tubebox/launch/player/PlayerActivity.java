package com.tubemate.tubebox.launch.player;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.util.BaseActivity;

public class PlayerActivity extends BaseActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
    }
}
