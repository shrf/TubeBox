package com.tubemate.tubebox.util;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.tubemate.tubebox.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    public void finish() {
        super.finish();
//        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
